format PE console
entry start

section '.text' code executable

start:
        xor eax,eax
        xor edx,edx
        xor ecx,ecx
        xor ebx,ebx

        mov ebx,[fs:0x30] ; peb pointer
        mov ebx,[ebx+0x008] ;peb->ImageBaseAddress

        mov edi,ebx
        add edi,0x1000+0x29 ;oep will be provided in arg (0x1000) 0x2d is the length of loader in order to work directly on the shellcode
        mov cl,25   ;shellcode length  - counter loop

        mov dl,[edi]
        mov al,0x42 ;key value
        xor al,dl ;result in al
        mov [edi],al
        inc edi
        dec cl
        jnz $-0xb ;relative offset to jump back

        ;test futur shellcode
        ;add bl,1
        ;add al,0x42
        ;xor bl,0xF
